<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');


$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');


/* Vypis osob */
$app->get('/persons', function (Request $request, Response $response, $args) {
	// V prom�nn� stmt je ulo�en� v�sledek dotazu jako objekt datab�zov�ho konektoru 
	$stmt = $this->db->query('SELECT * FROM person ORDER BY first_name');

	// Je pot�eba pr�ev�stt na datovou strukturu jazyka php
	$tplVars['osoby'] = $stmt->fetchAll();
	/*
	osoby => [
		[first_name => "Pepa", last_name => "Novak", "height" => 156]
		[first_name => "Jan", last_name => "B", "height" => 156]
	]
	*/
    return $this->view->render($response, 'persons.latte', $tplVars);
})->setName('persons');


/* zobrazen� formul��e pro p�id�n� nov� osoby */
$app->get('/persons/new', function (Request $request, Response $response, $args) {
	$tplVars['formData'] = [
		'first_name' => '',
		'last_name' => '',
		'nickname' => '',
		'id_location' => null,
		'gender' => '',
		'height' => '',
		'birth_day' => '',
		'city' => '',
		'street_name' => '',
		'street_number' => '',
		'zip' => ''
	];
	return $this->view->render($response, 'newPerson.latte', $tplVars);
})->setName('newPerson');


function insert_location($db, $formData) {
	try {
		$stmt = $db->prepare("INSERT INTO location (city, street_name, street_number, zip) VALUES (:city, :street_name, :street_number, :zip)");
		$stmt->bindValue(":city", empty($formData['city']) ? null : $formData['city']);
		$stmt->bindValue(":street_number", empty($formData['street_number']) ? null : $formData['street_number']);
		$stmt->bindValue(":street_name", empty($formData['street_name']) ? null : $formData['street_name']);
		$stmt->bindValue(":zip", empty($formData['zip']) ? null : $formData['zip']);
		$stmt->execute();
		return $db->lastInsertId('location_id_location_seq');
	} catch (PDOException $e) {
		echo $e->getMessage();
	}
} 



/* odesl�n� formul��e a zpracov�n� dat */
$app->post('/persons/new', function (Request $request, Response $response, $args) {
	$formData = $request->getParsedBody();
	if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname'])) {
		$tplVars['message'] = 'Fill required fields';
	} else {
		$id_location = null;
		if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) || !empty($formData['zip'])) {
			$id_location = insert_location($this->db, $formData);
		}

		try {
			$stmt = $this->db->prepare("INSERT INTO person (nickname, first_name, last_name, id_location, birth_day, height, gender)
									VALUES (:nickname, :first_name, :last_name, :id_location, :birth_day, :height, :gender)");

			$stmt->bindValue(":nickname", $formData['nickname']); //   \;DROP TABLE person\;
			$stmt->bindValue(":first_name", $formData['first_name']); 
			$stmt->bindValue(":last_name", $formData['last_name']); 
			$stmt->bindValue(":id_location", $id_location); 
			$stmt->bindValue(":gender", empty($formData['gender']) ?  null : $formData['gender']); 
			$stmt->bindValue(":height", !empty($formData['height']) ? $formData['height'] : null); 
			$stmt->bindValue(":birth_day", !empty($formData['birth_day']) ? $formData['birth_day'] : null); 

			$stmt->execute();
			$tplVars['message'] = 'Person succesfully inserted';
		} catch (PDOException $e){
			$tplVars['message'] = 'Sorry, error occured';
			$this->logger->error($e->getMessage());

		}
	}
	$tplVars['formData'] = $formData;
	return $this->view->render($response, 'newPerson.latte', $tplVars);

});


/* Editace uzivatele */
$app->get('/persons/update', function (Request $request, Response $response, $args) {
	$params = $request->getQueryParams(); // persons/update?id_person=55&gender=male ===> $params = ['id_person' => '55', 'gender' => 'male' ]
	if (empty($params['id_person'])) {
		exit('id person is missing');
	} else {
		$stmt = $this->db->prepare("SELECT * FROM person LEFT JOIN location ON (person.id_location = location.id_location) WHERE id_person = :id_person");
		$stmt->bindValue(":id_person", $params['id_person']);
		$stmt->execute();
		$tplVars['formData'] = $stmt->fetch();
		if (empty($tplVars['formData'])) {
			exit('person not found');
		} else {
			return $this->view->render($response, 'updatePerson.latte', $tplVars);
		}
	}

})->setName('persons_update');


/* Maz�n� osoby */
$app->post('/persons/delete', function (Request $request, Response $response, $args) {
	$params = $request->getQueryParams(); // persons/update?id_person=55
	if (empty($params['id_person'])) {
		exit('id person is missing'); 
	} else {
		try {
			$stmt = $this->db->prepare("DELETE FROM person WHERE id_person = :id_person");
			$stmt->bindValue(':id_person', $params['id_person']);
			$stmt->execute();
		} catch (PDOException $e) {
			$this->logger->info($e);
		}
	}
	return $response->withHeader('Location', $this->router->pathFor('persons'));

})->setName('person_delete');